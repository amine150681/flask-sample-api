from flask import request
from flask_restful import Resource

from models import ItemModel
from schemas import ItemSchema


ITEM_NOT_FOUND = "Item not found."


item_schema = ItemSchema()
item_list_schema = ItemSchema(many=True)


class Item(Resource):

    def get(self, id):
        item_data = ItemModel.find_by_id(id)
        if item_data:
            return item_schema.dump(item_data)
        return {'message': 'Item not found'}, 404

    def delete(self,id):
        item_data = ItemModel.find_by_id(id)
        if item_data:
            item_data.delete()
            return {'message': "Item Deleted successfully"}, 200
        return {'message': ITEM_NOT_FOUND}, 404


class ItemList(Resource):
    def get(self):
        return item_list_schema.dump(ItemModel.find_all()), 200

