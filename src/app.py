from flask import Flask, Blueprint
from flask_restful import Api
from settings import marshmllw, db

from resources.item import Item, ItemList
from marshmallow import ValidationError

app = Flask(__name__)
api = Api(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///data.db'
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['PROPAGATE_EXCEPTIONS'] = True


api.add_resource(Item, '/items/<int:id>')
api.add_resource(ItemList, "/items")


def populate_db():
    from models import ItemModel
    it1 = ItemModel(name='Cookie', price=1.79)
    it1.save()
    it2 = ItemModel(name='Chocolate', price=4.50)
    it2.save()


def create_app():
    db.init_app(app)
    marshmllw.init_app(app)
    app.app_context().push()
    db.create_all()
    populate_db()
    return app

